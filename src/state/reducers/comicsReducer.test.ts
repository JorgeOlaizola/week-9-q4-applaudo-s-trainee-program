/* eslint-disable no-undef */
import { comics } from '../actions/actionsTypes';
import comicsReducer from './comicsReducer';

const initialState = {
  comics: [],
  comicsCopy: [],
};

const populatedComics = [
  { name: 'testOne', img: 'imgTestOne', id: 1 },
  { name: 'testTwo', img: 'imgTestTwo', id: 2 },
  { name: 'testThree', img: 'imgTestThree', id: 3 },
];

describe('Comics reducer', () => {
  test('Should return the initial state', () => {
    expect(comicsReducer(undefined, {})).toEqual(initialState);
  });
});

describe('Set comics', () => {
  test('Should set comics and its copies', () => {
    const action = {
      type: comics.SET_COMICS,
      payload: populatedComics,
    };
    expect(comicsReducer(initialState, action)).toEqual({
      comics: populatedComics,
      comicsCopy: populatedComics,
    });
  });

  test('Should set ONLY characters when searching', () => {
    const action = {
      type: comics.SEARCH,
      payload: populatedComics,
    };
    expect(comicsReducer(initialState, action)).toEqual({
      comics: populatedComics,
      comicsCopy: [],
    });
  });
});
