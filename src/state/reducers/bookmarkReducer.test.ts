/* eslint-disable no-undef */
import { bookmarks } from '../actions/actionsTypes';
import bookmarksReducer from './bookmarkReducer';

const initialState = {
  characters: [],
  stories: [],
  comics: [],
};

describe('Bookmarks reducer', () => {
  test('Should return the initial state', () => {
    expect(bookmarksReducer(undefined, {})).toEqual(initialState);
  });
});

describe('Add bookmarks', () => {
  test('Should add characters bookmarks', () => {
    const action = {
      type: bookmarks.ADD_BOOKMARK_CHARACTER,
      payload: {
        id: 1,
        name: 'Test bookmark',
        source: 'test.source.com',
        description: '',
        img: 'img',
        comics: [],
        stories: [],
      },
    };
    expect(bookmarksReducer(undefined, action)).toEqual({
      ...initialState,
      characters: [action.payload],
    });
  });

  test('Should add comics bookmarks', () => {
    const action = {
      type: bookmarks.ADD_BOOKMARK_COMIC,
      payload: {
        id: 1,
        title: 'Test bookmark',
        source: 'test.source.com',
        description: '',
        img: 'img',
        characters: [],
        stories: [],
      },
    };
    expect(bookmarksReducer(undefined, action)).toEqual({
      ...initialState,
      comics: [action.payload],
    });
  });

  test('Should add stories bookmarks', () => {
    const action = {
      type: bookmarks.ADD_BOOKMARK_STORY,
      payload: {
        id: 1,
        title: 'Test bookmark',
        source: 'test.source.com',
        description: '',
        img: 'img',
        characters: [],
        comics: [],
      },
    };
    expect(bookmarksReducer(undefined, action)).toEqual({
      ...initialState,
      stories: [action.payload],
    });
  });
});

describe('Remove bookmarks', () => {
  const character = {
    id: 1,
    name: 'Test bookmark',
    source: 'test.source.com',
    description: '',
    img: 'img',
    comics: [],
    stories: [],
  };
  const comic = {
    id: 1,
    title: 'Test bookmark',
    source: 'test.source.com',
    description: '',
    img: 'img',
    characters: [],
    stories: [],
  };
  const story = {
    id: 1,
    title: 'Test bookmark',
    source: 'test.source.com',
    description: '',
    img: 'img',
    characters: [],
    comics: [],
  };
  const initialState = {
    characters: [character],
    stories: [story],
    comics: [comic],
  };

  test('Should remove characters bookmarks', () => {
    const action = {
      type: bookmarks.REMOVE_BOOKMARK_CHARACTER,
      payload: 1,
    };
    expect(bookmarksReducer(initialState, action)).toEqual({ ...initialState, characters: [] });
  });

  test('Should remove comics bookmarks', () => {
    const action = {
      type: bookmarks.REMOVE_BOOKMARK_COMIC,
      payload: 1,
    };
    expect(bookmarksReducer(initialState, action)).toEqual({ ...initialState, comics: [] });
  });

  test('Should remove stories bookmarks', () => {
    const action = {
      type: bookmarks.REMOVE_BOOKMARK_STORY,
      payload: 1,
    };
    expect(bookmarksReducer(initialState, action)).toEqual({ ...initialState, stories: [] });
  });
});
