/* eslint-disable no-undef */
import { characters } from '../actions/actionsTypes';
import charactersReducer from './charactersReducer';

const initialState = {
  characters: [],
  charactersCopy: [],
};

const populatedCharacters = [
  { name: 'testOne', img: 'imgTestOne', id: 1 },
  { name: 'testTwo', img: 'imgTestTwo', id: 2 },
  { name: 'testThree', img: 'imgTestThree', id: 3 },
];

describe('Characters reducer', () => {
  test('Should return the initial state', () => {
    expect(charactersReducer(undefined, {})).toEqual(initialState);
  });
});

describe('Set characters', () => {
  test('Should set characters and its copies', () => {
    const action = {
      type: characters.SET_CHARACTERS,
      payload: populatedCharacters,
    };
    expect(charactersReducer(initialState, action)).toEqual({
      characters: populatedCharacters,
      charactersCopy: populatedCharacters,
    });
  });

  test('Should set ONLY characters when searching', () => {
    const action = {
      type: characters.SEARCH,
      payload: populatedCharacters,
    };
    expect(charactersReducer(initialState, action)).toEqual({
      characters: populatedCharacters,
      charactersCopy: [],
    });
  });
});
