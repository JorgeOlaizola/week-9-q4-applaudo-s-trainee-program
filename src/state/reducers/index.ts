import { combineReducers } from 'redux';
import bookmarksReducer from './bookmarkReducer';
import charactersReducer from './charactersReducer';
import comicsReducer from './comicsReducer';
import storiesReducer from './storiesReducer';

const reducers = combineReducers({
  characters: charactersReducer,
  comics: comicsReducer,
  bookmarks: bookmarksReducer,
  stories: storiesReducer,
});

export default reducers;

export type State = ReturnType<typeof reducers>
