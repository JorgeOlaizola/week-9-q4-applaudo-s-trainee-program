import { comics } from '../actions/actionsTypes';

const initialState = {
  comics: [],
  comicsCopy: []
};

const comicsReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case comics.SET_COMICS:
      return { ...state, comics: action.payload, comicsCopy: action.payload };
    case comics.SEARCH:
      return { ...state, comics: action.payload };
    default:
      return state;
  }
};

export default comicsReducer;
