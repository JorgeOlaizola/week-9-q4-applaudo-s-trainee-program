/* eslint-disable no-undef */
import { stories } from '../actions/actionsTypes';
import storiesReducer from './storiesReducer';

const initialState = {
  stories: [],
  storiesCopy: [],
};

const populatedStories = [
  { name: 'testOne', img: 'imgTestOne', id: 1 },
  { name: 'testTwo', img: 'imgTestTwo', id: 2 },
  { name: 'testThree', img: 'imgTestThree', id: 3 },
];

describe('Stories reducer', () => {
  test('Should return the initial state', () => {
    expect(storiesReducer(undefined, {})).toEqual(initialState);
  });
});

describe('Set Stories', () => {
  test('Should set Stories and its copies', () => {
    const action = {
      type: stories.SET_STORIES,
      payload: populatedStories,
    };
    expect(storiesReducer(initialState, action)).toEqual({
      stories: populatedStories,
      storiesCopy: populatedStories,
    });
  });

  test('Should set ONLY characters when searching', () => {
    const action = {
      type: stories.SEARCH,
      payload: populatedStories,
    };
    expect(storiesReducer(initialState, action)).toEqual({
      stories: populatedStories,
      storiesCopy: [],
    });
  });
});
