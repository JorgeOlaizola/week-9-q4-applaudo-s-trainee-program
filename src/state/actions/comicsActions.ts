import { Dispatch } from 'redux';
import TCard from '../../interfaces/card/TCard';
import { comics } from './actionsTypes';

export const dispatchComics = (payload: TCard[]) => {
  return (dispatch: Dispatch) => {
    dispatch({
      type: comics.SET_COMICS,
      payload,
    });
  };
};

export const searchComics = (payload: TCard[]) => {
  return (dispatch: Dispatch) => {
    dispatch({
      type: comics.SEARCH,
      payload,
    });
  };
};
