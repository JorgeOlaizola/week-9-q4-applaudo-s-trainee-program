import { Dispatch } from 'redux';
import TCard from '../../interfaces/card/TCard';
import { characters } from './actionsTypes';

export const dispatchCharacters = (payload: TCard[]) => {
  return (dispatch: Dispatch) => {
    dispatch({
      type: characters.SET_CHARACTERS,
      payload
    });
  };
};

export const searchCharacters = (payload: TCard[]) => {
  return (dispatch: Dispatch) => {
    dispatch({
      type: characters.SEARCH,
      payload
    });
  };
};
