type TCard = {
    name: string,
    id: number,
    img: string
}

export default TCard;
