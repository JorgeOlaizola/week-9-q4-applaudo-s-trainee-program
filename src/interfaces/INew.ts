/* eslint-disable semi */
type TDisplay = 'right' | 'left';

export default interface INew {
  title: string;
  content: string;
  image: string;
  display: TDisplay;
}
