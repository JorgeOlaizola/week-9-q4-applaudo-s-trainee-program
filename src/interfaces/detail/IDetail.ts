interface IDetail {
    name?: string;
    title?: string;
    id?: number;
    img?: string;
    comics?: TArrayComsStories[];
    characters?: TArrayComsStories[];
    stories?: TArrayComsStories[];
    source?: string;
    description?: string | null;
}

type TArrayComsStories = {
    name: string;
    resourceURI: string;
}

export default IDetail;
