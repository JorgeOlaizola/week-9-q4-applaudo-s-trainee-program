import React from 'react';
import { useRoutes } from 'react-router-dom';
import LandingLayout from './Layouts/LandingLayout';
import MainLayout from './Layouts/MainLayout';
import Bookmarks from './pages/Bookmarks';
import Characters from './pages/Characters';
import Comics from './pages/Comics';
import Detail from './pages/Detail';
import Home from './pages/Home';
import Landing from './pages/Landing';
import Stories from './pages/Stories';
import { routes } from './utils/routes';

function App() {
  const mainRoutes = {
    path: '/',
    element: <MainLayout />,
    children: [
      { path: routes.home, element: <Home /> },
      { path: routes.characters, element: <Characters /> },
      { path: routes.stories, element: <Stories /> },
      { path: routes.comics, element: <Comics /> },
      { path: routes.bookmarks, element: <Bookmarks /> },
      { path: routes.charactersDetail, element: <Detail type='characters' /> },
      { path: routes.comicsDetail, element: <Detail type='comics' /> },
      { path: routes.storiesDetail, element: <Detail type='stories' /> },
    ],
  };

  const landingRoute = {
    path: '/',
    element: <LandingLayout />,
    children: [
      { path: routes.landing, element: <Landing /> },
    ],
  };

  const routesContainer = useRoutes([landingRoute, mainRoutes]);
  return <>{routesContainer}</>;
}

export default App;
