export const routes = {
  landing: '/',
  home: '/home',
  characters: '/characters',
  charactersDetail: '/characters/:id',
  comics: '/comics',
  comicsDetail: '/comics/:id',
  stories: '/stories',
  storiesDetail: '/stories/:id',
  bookmarks: '/bookmarks'
};
