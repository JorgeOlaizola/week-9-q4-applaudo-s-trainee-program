import INew from '../interfaces/INew';

const newOne: INew = {
  title: 'Head to the Beginning with the Eternals',
  content:
    'The Marvel Cinematic Universe’s newest Super Heroes hit the Big Screen on Friday! To celebrate, a brand-new, action-packed TV spot for Marvel Studios’ Eternals debuted today, above. Be among the first to witness these heroes in action! Tickets are on-sale now: Fandango.com/Eternals!Eternals follows a group of heroes from beyond the stars who had protected the Earth since the dawn of man. When monstrous creatures called the Deviants, long thought lost to history, mysteriously return, the Eternals are forced to reunite in order to defend humanity once again. The outstanding ensemble cast includes Gemma Chan as humankind-loving Sersi, Richard Madden as the all-powerful Ikaris, Kumail Nanjiani as cosmic-powered Kingo, Lia McHugh as the eternally young, old-soul Sprite, Brian Tyree Henry as the intelligent inventor Phastos, Lauren Ridloff as the super-fast Makkari, Barry Keoghan as aloof loner Druig, Don Lee as the powerful Gilgamesh, with Kit Harington as Dane Whitman, with Salma Hayek as the wise and spiritual leader Ajak, and Angelina Jolie as the fierce warrior Thena.  ',
  image: 'https://fandomwire.com/wp-content/uploads/2020/08/4-13.jpg',
  display: 'right',
};

const newTwo: INew = {
  title: 'The Love Life of Aunt May',
  content: 'Have you tried Marvel Unlimited yet? It’s your all-access pass to over 29,000 Marvel comics, available at your fingertips. Download our all-new, supercharged app on the App Store or Google Play today and save 50% off your first month! If life is fleeting, then love and happiness can be even more evanescent—especially in a comics universe home to the likes of reality-bending characters like Wanda Maximoff and planet-eating gods like Galactus. And this doesn’t even cover regular day-to-day happenings, which are even smaller in scale and can be just as life altering to those intimately affected. You could fall in love, and in a blink, its gone, but you somehow manage to keep living, learning to manage the love persevering in grief. May Parker, AKA Spider-Man’s Aunt May, knows all about it.',
  image: 'https://static3.cbrimages.com/wordpress/wp-content/uploads/2019/09/may-feature.jpg',
  display: 'left',
};

const newThree: INew = {
  title: 'Marvel Games Comic Connection: Gamora',
  content: 'Hello, True Believers! Welcome to the latest installment of Marvel Games Comic Connection, where your pals at Marvel Games search through the House of Ideas’ cavernous Comic Book Vault to spotlight the incredible issues that inspire the amazing additions of the coolest characters hitting our most popular video games. This week’s stellar installment celebrates the recent worldwide launch of Marvel’s Guardians of the Galaxy, which GamingTrend.com calls “the biggest surprise of the year, and joins Marvel’s Spider-Man as one of the best Marvel video games in the last decade.” Created by the mad geniuses at Eidos-Montreal, this single-player action-adventure game takes you from Knowhere to the furthest reaches of the universe as our beloved rag-tag band of underdogs attempt to pay the bills, stay alive and—oh, yeah—save the galaxy! Continuing the Marvel Games tradition of celebrating classic comic book characters by reimagining them for new medium, Marvel’s Guardians of the Galaxy features a unique version of Gamora, the former assassin raised by the infamous Thanos, which has players thrilled and cosplayers running to their workshops. So just who is this skilled swordswoman and what drives her to carve a path of vengeance through the stars? Let’s leap into this week’s cosmic Comic Connection!',
  image: 'https://th.bing.com/th/id/R.79fd58c9bd983d5f4795d8add37f9b0a?rik=e2vApdXbZQbSdQ&riu=http%3a%2f%2fwww.fightersgeneration.com%2fnf%2fchar%2fgamora-guardians2.jpg&ehk=lblpaxBqnrwGq2K%2bW%2byrqSuo4%2beg%2ffuQBx3ETmFHCjs%3d&risl=&pid=ImgRaw&r=0',
  display: 'right',
};

export default [newOne, newTwo, newThree];
