export const detailCharacter = {
  description: 'testDescription',
  id: 1,
  name: 'testOne',
  img: 'string',
  source: 'string',
  stories: [{ name: 'test', resourceURI: 'test', type: 'string' }],
  comics: [{ name: 'test', resourceURI: 'test' }],
};

export const detailComic = {
  description: 'testDescription',
  id: 1,
  title: 'testOne',
  img: 'string',
  source: 'string',
  stories: [{ name: 'test', resourceURI: 'test', type: 'string' }],
  characters: [{ name: 'test', resourceURI: 'test' }],
};

export const detailStory = {
  description: 'testDescription',
  id: 1,
  title: 'testOne',
  img: 'string',
  source: 'string',
  comics: [{ name: 'test', resourceURI: 'test', type: 'string' }],
  characters: [{ name: 'test', resourceURI: 'test' }],
};

const imageObject = {
  path: 'testPath',
  extension: 'testExtension',
};

const arrayForTest = {
  available: true,
  items: ['test'],
};

const characterResponse = {
  data: {
    results: [
      {
        description: 'testDescription',
        id: 1,
        name: 'testOne',
        img: 'string',
        urls: [{ url: 'testUrl' }],
        thumbnail: imageObject,
        comics: arrayForTest,
        stories: arrayForTest,
      },
    ],
  },
};

const responses = {
  characters: characterResponse,
};

export default responses;
