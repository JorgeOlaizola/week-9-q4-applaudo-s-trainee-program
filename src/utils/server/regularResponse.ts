const testImg = {
  path: 'testPath',
  extension: '.test',
};

const testArray = [
  { name: 'testOne', thumbnail: testImg, id: 1 },
  { name: 'testTwo', thumbnail: testImg, id: 2 },
  { name: 'testThree', thumbnail: testImg, id: 3 },
];

const testArrayTwo = [
  { title: 'testOne', thumbnail: testImg, id: 1 },
  { title: 'testTwo', thumbnail: testImg, id: 2 },
  { title: 'testThree', thumbnail: testImg, id: 3 },
];

const testResponse = {
  data: {
    results: testArray,
  },
};

const testResponseTwo = {
  data: {
    results: testArrayTwo,
  },
};

export default { testResponse, testResponseTwo };
