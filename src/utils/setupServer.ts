/* eslint-disable no-undef */
import { setupServer } from 'msw/node';
import { rest } from 'msw';
import { REQUEST_URL } from './env';
import response from './server/regularResponse';
import responses from './server/detailResponse';

const server = setupServer(
  rest.get(REQUEST_URL('characters'), (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(response.testResponse));
  }),
  rest.get(REQUEST_URL('characters/1'), (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(responses.characters));
  }),
  rest.get(REQUEST_URL('comics'), (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(response.testResponseTwo));
  }),
  rest.get(REQUEST_URL('comics/1'), (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(responses.characters));
  }),
  rest.get(REQUEST_URL('stories'), (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(response.testResponseTwo));
  }),
  rest.get(REQUEST_URL('stories/1'), (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(responses.characters));
  }),
  rest.get('http://someUrlToThrow.com', (req, res, ctx) => {
    return res(ctx.status(400));
  })
);

beforeAll(() => server.listen());
afterAll(() => server.close());
afterEach(() => server.resetHandlers());

export { server, rest };
