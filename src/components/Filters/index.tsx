import React from 'react';
import SearchbarCharacters from './SearchbarCharacters';
import SearchbarStories from './SearchbarStories';
import SearchbarComics from './SearchbarComics';
import './filters.scss';
import TDetail from '../../interfaces/detail/TDetail';

export default function index({ type }: { type: TDetail }) {
  return (
    <div className='filters-container'>
      <div className='filters-item'>
        {type === 'characters' && <SearchbarCharacters />}
        {type === 'comics' && <SearchbarComics />}
        {type === 'stories' && <SearchbarStories />}
      </div>
    </div>
  );
}
