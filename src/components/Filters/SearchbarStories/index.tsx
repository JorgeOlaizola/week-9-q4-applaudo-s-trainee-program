import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { State } from '../../../state/reducers';
import '../SearchbarCharacters/searchbar.scss';
import TCard from '../../../interfaces/card/TCard';
import { searchStories } from '../../../state/actions/storiesActions';

export default function index() {
  const state = useSelector((state: State) => state.stories.storiesCopy);
  const dispatch = useDispatch();

  const changeHandler = (input: string) => {
    dispatch(
      searchStories(
        state.filter((item: TCard) => item.name.toLowerCase().includes(input.toLowerCase()))
      )
    );
  };
  return (
    <div className='searchbar-container'>
      <input
        className='searchbar-input'
        onChange={(e) => changeHandler(e.target.value)}
        type='text'
        placeholder='Search'
      />
    </div>
  );
}
