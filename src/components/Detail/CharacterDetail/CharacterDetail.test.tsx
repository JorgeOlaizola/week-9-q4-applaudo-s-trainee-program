/* eslint-disable no-undef */
import React from 'react';
import { Provider } from 'react-redux';
import '@testing-library/jest-dom/extend-expect';
import { fireEvent } from '@testing-library/react';
import renderWithRouter from '../../../utils/Wrapper';
import CharactersDetail from './index';
import { detailCharacter } from '../../../utils/server/detailResponse';
import { store } from '../../../state/store';

describe('Characters detail', () => {
  test('Should render properly', () => {
    const characterDetail = renderWithRouter(
      <Provider store={store}>
        <CharactersDetail detail={detailCharacter} />
      </Provider>
    );
    expect(characterDetail.container).toHaveTextContent('testOne');
  });
  test('Should add bookmarks', () => {
    const { container, queryByText, debug } = renderWithRouter(
      <CharactersDetail detail={detailCharacter} />
    );
    expect(container).toHaveTextContent('Add to bookmarks 🧡');
    fireEvent.click(container.getElementsByClassName('detail-button-add')[0]!);
    expect(container).toHaveTextContent('Remove bookmark ❌');
  });
});
