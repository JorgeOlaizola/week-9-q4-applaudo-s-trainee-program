import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link, useNavigate } from 'react-router-dom';
import { removeBookmark, addBookmark } from '../../../state/actions/bookmarksActions';
import { State } from '../../../state/reducers';
import './storyDetail.scss';

interface IComics {
  name: string;
  resourceURI: string;
}

interface ICharacters {
  name: string;
  resourceURI: string;
}

interface IComicDetail {
  description: string | null;
  id: number;
  title: string;
  source: string;
  comics: IComics[] | null;
  characters: ICharacters[];
}

export default function index({ detail }: { detail: IComicDetail }) {
  const navigate = useNavigate();
  const bookmarks = useSelector((state: State) => state.bookmarks.stories);
  const [isBookmark, setBookmark] = useState(false);
  const dispatch = useDispatch();

  useEffect(() => {
    bookmarks.forEach((bookmark) => {
      if (bookmark.id === detail.id) setBookmark(true);
    });
  }, [bookmarks]);

  const clickRemove = () => {
    dispatch(removeBookmark('stories', detail.id));
    setBookmark(false);
  };

  return (
    <div className='detail-container'>
      <Link to='/stories' className='detail-button-back'>
        ⬅ Back
      </Link>
      <h1 className='detail-title'>{detail && detail.title}</h1>
      {detail && detail.description && <p className='detail description'>{detail.description}</p>}
      {detail && detail.characters && <h3 className='detail-header'>Characters</h3>}
      {detail &&
        detail.characters &&
        detail.characters.map((char) => {
          const charId = char.resourceURI.split('/');
          return (
            <div className='detail-info-item'>
              <Link className='detail-info-link' to={`/stories/${charId[charId.length - 1]}`}>
                {char.name}
              </Link>
            </div>
          );
        })}
      {detail && detail.comics && <h3 className='detail-header'>Comics</h3>}
      {detail &&
        detail.comics &&
        detail.comics.map((comic) => {
          const comicId = comic.resourceURI.split('/');
          return (
            <div className='detail-info-item'>
              <div
                className='detail-info-link'
                onClick={() => navigate(`/characters/${comicId[comicId.length - 1]}`)}
              >
                {comic.name}
              </div>
            </div>
          );
        })}
      {isBookmark ? (
        <button className='detail-button detail-button-remove' type='button' onClick={clickRemove}>
          Remove bookmark ❌
        </button>
      ) : (
        <button
          className='detail-button detail-button-add'
          type='button'
          onClick={() => dispatch(addBookmark('stories', detail))}
        >
          Add to bookmarks 🧡
        </button>
      )}
      <button className='detail-button detail-button-source' type='button'>
        <a className='detail-button-source-content' href={detail && detail.source}>
          {' '}
          Go to source 🔼{' '}
        </a>
      </button>
    </div>
  );
}
