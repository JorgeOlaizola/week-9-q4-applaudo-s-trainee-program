/* eslint-disable no-undef */
import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { fireEvent } from '@testing-library/react';
import renderWithRouter from '../../../utils/Wrapper';
import StoryDetail from './index';
import { detailStory } from '../../../utils/server/detailResponse';

describe('Story detail', () => {
  test('Should render properly', () => {
    const storyDetail = renderWithRouter(<StoryDetail detail={detailStory} />);
    expect(storyDetail.container).toHaveTextContent('testOne');
  });
  test('Should add bookmarks', () => {
    const { container } = renderWithRouter(
      <StoryDetail detail={detailStory} />
    );
    expect(container).toHaveTextContent('Add to bookmarks 🧡');
    fireEvent.click(container.getElementsByClassName('detail-button-add')[0]!);
    expect(container).toHaveTextContent('Remove bookmark ❌');
  });
});
