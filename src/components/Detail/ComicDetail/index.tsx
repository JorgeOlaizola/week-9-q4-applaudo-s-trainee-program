import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useNavigate } from 'react-router-dom';
import './comicDetail.scss';
import '../detailStyles.scss';
import { State } from '../../../state/reducers';
import { removeBookmark, addBookmark } from '../../../state/actions/bookmarksActions';

interface IStories {
  name: string;
  resourceURI: string;
  type: string;
}

interface ICharacters {
  name: string;
  resourceURI: string;
}

interface IComicDetail {
  description: string | null;
  id: number;
  title: string;
  img: string;
  source: string;
  stories: IStories[] | null;
  characters: ICharacters[];
}

export default function index({ detail }: { detail: IComicDetail }) {
  const navigate = useNavigate();
  const bookmarks = useSelector((state: State) => state.bookmarks.comics);
  const [isBookmark, setBookmark] = useState(false);
  const dispatch = useDispatch();

  useEffect(() => {
    bookmarks.forEach((bookmark) => {
      if (bookmark.id === detail.id) setBookmark(true);
    });
  }, [bookmarks]);

  const clickRemove = () => {
    dispatch(removeBookmark('comics', detail.id));
    setBookmark(false);
  };

  return (
    <div className='detail-container'>
      <Link to='/comics' className='detail-button-back'>
        ⬅ Back
      </Link>
      <h1 className='detail-title'>{detail && detail.title}</h1>
      <img className='detail-img' src={detail && detail.img} alt='detail' />
      {detail && detail.description && <p className='detail description'>{detail.description}</p>}
      {detail && detail.stories && <h3 className='detail-header'>Stories</h3>}
      {detail &&
        detail.stories &&
        detail.stories.map((story) => {
          const storyId = story.resourceURI.split('/');
          return (
            <div className='detail-info-item'>
              <Link className='detail-info-link' to={`/stories/${storyId[storyId.length - 1]}`}>
                {story.name}
              </Link>
            </div>
          );
        })}
      {detail && detail.characters && <h3 className='detail-header'>Characters</h3>}
      {detail &&
        detail.characters &&
        detail.characters.map((char) => {
          const charId = char.resourceURI.split('/');
          return (
            <div className='detail-info-item'>
              <div
                className='detail-info-link'
                onClick={() => navigate(`/characters/${charId[charId.length - 1]}`)}
              >
                {char.name}
              </div>
            </div>
          );
        })}
      {isBookmark ? (
        <button className='detail-button detail-button-remove' type='button' onClick={clickRemove}>
          Remove bookmark ❌
        </button>
      ) : (
        <button
          className='detail-button detail-button-add'
          type='button'
          onClick={() => dispatch(addBookmark('comics', detail))}
        >
          Add to bookmarks 🧡
        </button>
      )}
      <button className='detail-button detail-button-source' type='button'>
        <a className='detail-button-source-content' href={detail && detail.source}>
          {' '}
          Go to source 🔼{' '}
        </a>
      </button>
    </div>
  );
}
