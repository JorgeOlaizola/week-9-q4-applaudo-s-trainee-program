/* eslint-disable no-undef */
import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { fireEvent } from '@testing-library/react';
import renderWithRouter from '../../../utils/Wrapper';
import ComicDetail from './index';
import { detailComic } from '../../../utils/server/detailResponse';

describe('Comic detail', () => {
  test('Should render properly', () => {
    const comicDetail = renderWithRouter(<ComicDetail detail={detailComic} />);
    expect(comicDetail.container).toHaveTextContent('testOne');
  });
  test('Should add bookmarks', () => {
    const { container, queryByText, debug } = renderWithRouter(
      <ComicDetail detail={detailComic} />
    );
    expect(container).toHaveTextContent('Add to bookmarks 🧡');
    fireEvent.click(container.getElementsByClassName('detail-button-add')[0]!);
    expect(container).toHaveTextContent('Remove bookmark ❌');
  });
});
