import React, { memo } from 'react';
import './new.scss';
import INew from '../../../interfaces/INew';

function New({
  title,
  content,
  image,
  display
}: INew) {
  return display === 'right' ? (
    <div className='new-container'>
      <h3 className='new-title'>{title}</h3>
      <div className='new-info'>
        <div className='new-info-content'>{content}</div>
        <img className='new-info-image' src={image} alt='new' />
      </div>
    </div>
  ) : (
    <div className='new-container'>
      <h3 className='new-title'>{title}</h3>
      <div className='new-info'>
        <img className='new-info-image' src={image} alt='new' />
        <div className='new-info-content'>{content}</div>
      </div>
    </div>
  );
}

export default memo(New);
