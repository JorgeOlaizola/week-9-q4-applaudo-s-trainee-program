/* eslint-disable no-undef */
import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import renderWithRouter from '../../../utils/Wrapper';
import New from './New';

test('Should display information about the New', () => {
  const newCont = renderWithRouter(<New title='Test title' content='Lorem ipsum' image='' display='right' />);
  expect(newCont.container).toHaveTextContent('Test title');
  expect(newCont.container).toHaveTextContent('Lorem ipsum');
});

test('Should receive the display by props', () => {
  const newCont = renderWithRouter(<New title='Test title' content='Lorem ipsum' image='' display='left' />);
});
