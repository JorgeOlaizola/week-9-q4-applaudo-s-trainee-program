import React, { memo } from 'react';
import { Link } from 'react-router-dom';
import { routes } from '../../utils/routes';
import './navbar.scss';

function Navbar() {
  return (
    <nav className='nav'>
      <Link to={routes.landing}>
        <img
          className='nav-logo'
          src='https://th.bing.com/th/id/R.b9774cc4f99f8aa7fdcfe52a819e0fea?rik=wCYKzClBQ18emg&pid=ImgRaw&r=0'
          alt='logo'
        />
      </Link>
      <div className='nav-links'>
        <Link to={routes.home} className='nav-links-item'>
          Home
        </Link>
        <Link to={routes.characters} className='nav-links-item'>
          Characters
        </Link>
        <Link to={routes.comics} className='nav-links-item'>
          Comics
        </Link>
        <Link to={routes.stories} className='nav-links-item'>
          Stories
        </Link>
      </div>
      <Link to={routes.bookmarks} className='nav-bookmarks'>
        Bookmarks 🧡
      </Link>
    </nav>
  );
}

export default memo(Navbar);
