/* eslint-disable no-undef */
import React from 'react';
import { fireEvent, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import renderWithRouter from '../../utils/Wrapper';
import Navbar from './Navbar';

describe('Navbar', () => {
  test('Should render properly', () => {
    const { container } = renderWithRouter(<Navbar />);
    expect(container).toHaveTextContent('Home');
    expect(container).toHaveTextContent('Characters');
    expect(container).toHaveTextContent('Stories');
    expect(container).toHaveTextContent('Comics');
    expect(container).toHaveTextContent('Bookmarks 🧡');
  });
  test('Should navigate to Home', () => {
    const { getByText } = renderWithRouter(<Navbar />);
    fireEvent.click(getByText('Home'));
    expect(screen.getByTestId('location-display')).toHaveTextContent('/');
  });
  test('Should navigate to Characters', () => {
    const { getByText } = renderWithRouter(<Navbar />);
    fireEvent.click(getByText('Characters'), { click: 0 });
    expect(screen.getByTestId('location-display')).toHaveTextContent('/characters');
  });
  test('Should navigate to Stories', () => {
    const { getByText } = renderWithRouter(<Navbar />);
    fireEvent.click(getByText('Stories'), { click: 0 });
    expect(screen.getByTestId('location-display')).toHaveTextContent('/stories');
  });
  test('Should navigate to Comics', () => {
    const { getByText } = renderWithRouter(<Navbar />);
    fireEvent.click(getByText('Comics'), { click: 0 });
    expect(screen.getByTestId('location-display')).toHaveTextContent('/comics');
  });
  test('Should navigate to Bookmarks', () => {
    const { getByText } = renderWithRouter(<Navbar />);
    fireEvent.click(getByText('Bookmarks 🧡'), { click: 0 });
    expect(screen.getByTestId('location-display')).toHaveTextContent('/bookmarks');
  });
});
