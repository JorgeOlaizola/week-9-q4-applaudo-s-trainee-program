import React, { memo } from 'react';
import './footer.scss';

function Footer() {
  return (
    <footer className='footer'>
      <span>
        Developed by Jorge Olaizola. Make sure to follow my on{' '}
        <a className='footer-link' href='https://www.gitlab.com/JorgeOlaizola'>
          GitLab
        </a>
      </span>
    </footer>
  );
}

export default memo(Footer);
