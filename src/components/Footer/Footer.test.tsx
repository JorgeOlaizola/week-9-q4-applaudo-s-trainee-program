/* eslint-disable no-undef */
import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { render } from '@testing-library/react';
import Footer from './Footer';

test('Footer should render', () => {
  const footer = render(<Footer />);
  expect(footer.container).toHaveTextContent('Developed by Jorge Olaizola. Make sure to follow my on');
});
