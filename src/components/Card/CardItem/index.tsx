import React, { memo } from 'react';
import { useNavigate } from 'react-router-dom';
import TDetail from '../../../interfaces/detail/TDetail';
import './cardItem.scss';

function index({
  img,
  name,
  id,
  type,
}: {
  img: string;
  name: string;
  id: number | string;
  type: TDetail;
}) {
  const navigate = useNavigate();
  const detailRedirect = () => {
    navigate(`/${type}/${id}`);
  };
  return (
    <div onClick={detailRedirect} className='card-container'>
      <img src={img} alt={name} className='card-img' />
      <h3 className='card-title'>{name}</h3>
    </div>
  );
}

export default memo(index);
