/* eslint-disable no-undef */
import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { screen, fireEvent } from '@testing-library/react';
import renderWithRouter from '../../../utils/Wrapper';
import CardItem from './index';

describe('Card Item test', () => {
  test('Should render on /characters route', () => {
    renderWithRouter(<CardItem img='' name='Test' id={1} type='characters' />, '/characters');
    expect(screen.getByTestId('location-display')).toHaveTextContent('/characters');
  });

  test('Should redirect to detail when clicked', () => {
    const card = renderWithRouter(
      <CardItem img='' name='Test' id={1} type='characters' />,
      '/characters'
    );
    expect(screen.getByTestId('location-display')).toHaveTextContent('/characters');
    fireEvent.click(card.getByText('Test'), { click: 0 });
    expect(screen.getByTestId('location-display')).toHaveTextContent('/characters/1');
  });
});
