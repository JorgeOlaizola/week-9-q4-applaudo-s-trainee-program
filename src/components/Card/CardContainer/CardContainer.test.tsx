/* eslint-disable no-undef */
import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { screen, fireEvent } from '@testing-library/react';
import renderWithRouter from '../../../utils/Wrapper';
import CardContainer from './index';

test('Should render correctly', () => {
  const cardContainer = renderWithRouter(
    <CardContainer type='characters' items={[{ name: 'Test', img: '', id: 1 }]} />
  );
  expect(cardContainer.container).toHaveTextContent('Test');
});
