/* eslint-disable no-undef */
import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { fireEvent } from '@testing-library/react';
import Pagination from './Pagination';
import renderWithRouter from '../../utils/Wrapper';

describe('Pagination', () => {
  test('Should render properly', () => {
    const { container } = renderWithRouter(
      <Pagination
        itemsPerPage={1}
        totalItems={5}
        paginate={jest.fn()}
        noScroll={true}
        currentPage={1}
      />
    );
    expect(container).toHaveTextContent('1');
    expect(container).not.toHaveTextContent('999');
  });
  test('Should paginate correctly', () => {
    const mockPagination = jest.fn();
    const { container, getByText } = renderWithRouter(
      <Pagination
        itemsPerPage={1}
        totalItems={5}
        paginate={mockPagination}
        noScroll={true}
        currentPage={1}
      />
    );
    const button = getByText('1');
    fireEvent.click(button, { click: 0 });
    expect(mockPagination).toHaveBeenCalledTimes(1);
  });
});
