/* eslint-disable no-undef */
import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { fireEvent, RenderResult, screen } from '@testing-library/react';
import renderWithRouter from '../../../utils/Wrapper';
import StoriesItem from './index';

describe('Stories Item tests', () => {
  test('Should render with the right name', () => {
    const storiesItem = renderWithRouter(<StoriesItem name='Test Story' id={1} />);
    expect(storiesItem.container).toHaveTextContent('Test Story');
  });

  test('Should navigate to story detail when clicked', () => {
    const storiesItem = renderWithRouter(<StoriesItem name='Test Story' id={1} />);
    const clicker = storiesItem.getByText('Test Story');
    fireEvent.click(clicker, { click: 0 });
    expect(screen.getByTestId('location-display')).toHaveTextContent('/stories/1');
  });
});
