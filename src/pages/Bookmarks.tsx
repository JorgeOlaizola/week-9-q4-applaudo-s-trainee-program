import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import '../styles/cardPages.scss';
import '../styles/bookmarks.scss';
import { State } from '../state/reducers';
import { clearBookmarks } from '../state/actions/bookmarksActions';
import CardItem from '../components/Card/CardItem';
import StoriesItem from '../components/Stories/StoriesItem';

export default function Bookmarks() {
  const bookmarks = useSelector((state: State) => state.bookmarks);
  const dispatch = useDispatch();
  return (
    <div className='bookmarks-container'>
      <h1 className='card-page-title'>Bookmarks</h1>
      {bookmarks.characters.length > 0 && (
        <div className='bookmarks-section'>
          <h3 className='bookmarks-section-title'>Characters</h3>
          <div className='bookmarks-section-cards'>{bookmarks.characters.map((char) => <CardItem img={char.img} name={char.name} id={char.id} type='characters' />)}</div>
        </div>
      )}
      {bookmarks.comics.length > 0 && (
        <div className='bookmarks-section'>
          <h3 className='bookmarks-section-title'>Comics</h3>
          <div className='bookmarks-section-cards'>{bookmarks.comics.map((comic) => <CardItem img={comic.img} name={comic.title} id={comic.id} type='comics' />)}</div>
        </div>
      )}
      {bookmarks.stories.length > 0 && (
        <div className='bookmarks-section'>
          <h3 className='bookmarks-section-title'>Stories</h3>
          <div className='bookmarks-section-cards'>{bookmarks.stories.map((story) => <StoriesItem name={story.title} id={story.id} />)}</div>

        </div>
      )}
      {bookmarks.characters.length === 0 &&
      bookmarks.comics.length === 0 &&
      bookmarks.stories.length === 0 ? (
        <h3 className='bookmarks-section-title'>You dont have any bookmarks right now.</h3>
        ) : (
          <button onClick={() => dispatch(clearBookmarks())} type='button' className='bookmarks-remove-all'>
            CLEAR BOOKMARKS ❌
          </button>
        )}
    </div>
  );
}
