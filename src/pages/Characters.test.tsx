/* eslint-disable no-undef */
import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { fireEvent } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { waitFor } from '@testing-library/dom';
import renderWithRouter from '../utils/Wrapper';
import Characters from './Characters';

describe('Characters page', () => {
  test('Should render correctly', () => {
    const characters = renderWithRouter(<Characters />);
    expect(characters.container).toHaveTextContent('Characters');
  });
  test('Should render cards', async () => {
    const { container } = renderWithRouter(<Characters />);
    await waitFor(() => {
      expect(container.querySelector('.card-container')).not.toBeNull();
    });
  });
  test('Should filter by search input', async () => {
    const { getByPlaceholderText, container } = renderWithRouter(<Characters />);
    expect(container).toHaveTextContent('testOne');
    expect(container).toHaveTextContent('testTwo');
    expect(container).toHaveTextContent('testThree');
    const input = getByPlaceholderText('Search');
    userEvent.type(input, 'testOne');
    expect(container).toHaveTextContent('testOne');
    expect(container).not.toHaveTextContent('testTwo');
    expect(container).not.toHaveTextContent('testThree');
  });
});
