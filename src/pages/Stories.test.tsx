/* eslint-disable no-undef */
import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { waitFor } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';
import renderWithRouter from '../utils/Wrapper';
import Stories from './Stories';

describe('Stories page', () => {
  test('Should render correctly', () => {
    const stories = renderWithRouter(<Stories />);
    expect(stories.container).toHaveTextContent('Stories');
  });
  test('Should render cards', async () => {
    const { container } = renderWithRouter(<Stories />);
    await waitFor(() => {
      expect(container.querySelector('.stories-container')).not.toBeNull();
      expect(container.querySelector('.story-container')).not.toBeNull();
    });
  });
  test('Should filter by search input', async () => {
    const { getByPlaceholderText, container } = renderWithRouter(<Stories />);
    await waitFor(() => {
      expect(container).toHaveTextContent('testOne');
      expect(container).toHaveTextContent('testTwo');
      expect(container).toHaveTextContent('testThree');
      const input = getByPlaceholderText('Search');
      userEvent.type(input, 'testOne');
      expect(container).toHaveTextContent('testOne');
      expect(container).not.toHaveTextContent('testTwo');
      expect(container).not.toHaveTextContent('testThree');
    });
  });
});
