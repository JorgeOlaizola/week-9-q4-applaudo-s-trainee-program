import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import fetchDetail from '../API/fetchDetail';
import TDetail from '../interfaces/detail/TDetail';
import ComicDetail from '../components/Detail/ComicDetail';
import CharacterDetail from '../components/Detail/CharacterDetail';
import StoryDetail from '../components/Detail/StoryDetail';
import '../styles/detail.scss';
import IDetail from '../interfaces/detail/IDetail';

export default function Detail({ type }: { type: TDetail }) {
  const id = useParams();
  const [detail, setDetail] = useState<any>();
  useEffect(() => {
    const fetchFunction = async () => {
      const detailResponse = await fetchDetail(`${id.id}`, type);
      setDetail(detailResponse![0]);
    };
    fetchFunction();
  }, []);

  return (
    <div className='detail'>
      <div className='detail-container'>
        {detail && type === 'characters' && <CharacterDetail detail={detail} />}
        {detail && type === 'comics' && <ComicDetail detail={detail} />}
        {detail && type === 'stories' && <StoryDetail detail={detail} />}
      </div>
    </div>
  );
}
