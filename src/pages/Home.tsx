import React from 'react';
import New from '../components/Home/New/New';
import '../styles/home.scss';
import news from '../utils/news';
import INew from '../interfaces/INew';
import '../styles/cardPages.scss';

export default function Home() {
  return (
    <div className='home-container'>
      <h1 className='home-title'>Welcome to the Marvel App!</h1>
      <img
        className='home-image'
        src='https://upload.wikimedia.org/wikipedia/commons/thumb/b/b9/Marvel_Logo.svg/2560px-Marvel_Logo.svg.png'
        alt='home-img'
      />
      <p className='home-description'>
        Marvel Comics is the brand name and primary imprint of Marvel Worldwide Inc., formerly
        Marvel Publishing, Inc. and Marvel Comics Group, a publisher of American comic books and
        related media. In 2009, The Walt Disney Company acquired Marvel Entertainment, Marvel
        Worldwide&apos;s parent company. Marvel was started in 1939 by Martin Goodman under a number
        of corporations and imprints but now known as Timely Comics, and by 1951 had generally
        become known as Atlas Comics. The Marvel era began in 1961, the year that the company
        launched The Fantastic Four and other superhero titles created by Stan Lee, Jack Kirby,
        Steve Ditko and many others. The Marvel brand, which had been used over the years, was
        solidified as the company&apos;s primary brand.
      </p>
      <h1 className='card-page-title'>News</h1>
      <div className='home-news'>
        {news.map(({
          title,
          content,
          display,
          image
        }: INew) => (
          <New title={title} content={content} image={image} display={display} />
        ))}
      </div>
    </div>
  );
}
