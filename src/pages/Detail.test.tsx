/* eslint-disable no-undef */
import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { waitFor, screen } from '@testing-library/dom';
import renderWithRouter from '../utils/Wrapper';
import Detail from './Detail';
import fetchDetail from '../API/fetchDetail';

describe('Detail', () => {
  test('Should render characters detail', async () => {
    const detail = renderWithRouter(<Detail type='characters' />, '/characters/1');
    expect(detail.container.querySelector('.detail-title')).toBeNull();
    waitFor(() => {
      expect(detail.container.querySelector('.detail-title')).not.toBeNull();
    });
  });
  test('Should render comics detail', async () => {
    const detail = renderWithRouter(<Detail type='comics' />, '/comics/1');
    expect(detail.container.querySelector('.detail-title')).toBeNull();
    waitFor(() => {
      expect(detail.container.querySelector('.detail-title')).not.toBeNull();
    });
  });
  test('Should render stories detail', async () => {
    const detail = renderWithRouter(<Detail type='stories' />, '/stories/1');
    expect(detail.container.querySelector('.detail-title')).toBeNull();
    waitFor(() => {
      expect(detail.container.querySelector('.detail-title')).not.toBeNull();
    });
  });
});
