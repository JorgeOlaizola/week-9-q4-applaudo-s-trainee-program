/* eslint-disable no-undef */
import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import userEvent from '@testing-library/user-event';
import { waitFor } from '@testing-library/dom';
import renderWithRouter from '../utils/Wrapper';
import Comics from './Comics';
import { fetchComics } from '../API/fetchItems';

describe('Comics page', () => {
  test('Should render correctly', () => {
    const comics = renderWithRouter(<Comics />);
    expect(comics.container).toHaveTextContent('Comics');
  });
  test('Should render cards', async () => {
    const { container } = renderWithRouter(<Comics />);
    await waitFor(() => {
      expect(container.querySelector('.card-container')).not.toBeNull();
    });
  });
  test('Should filter by search input', async () => {
    const { getByPlaceholderText, container } = renderWithRouter(<Comics />);
    await waitFor(() => {
      expect(container).toHaveTextContent('testOne');
      expect(container).toHaveTextContent('testTwo');
      expect(container).toHaveTextContent('testThree');
      const input = getByPlaceholderText('Search');
      userEvent.type(input, 'testOne');
      expect(container).toHaveTextContent('testOne');
      expect(container).not.toHaveTextContent('testTwo');
      expect(container).not.toHaveTextContent('testThree');
    });
  });
});
