/* eslint-disable no-undef */
import { REQUEST_URL } from '../utils/env';
import { get } from './Fetch';
import responses from '../utils/server/regularResponse';

describe('Fetch helpers', () => {
  test('Should make a get request properly', async () => {
    const response: any = await get(REQUEST_URL('characters'));
    expect(response).toEqual(responses.testResponse);
  });
  test('Should throw an error when the request is bad', async () => {
    const testFunction = () => get('http://someUrlToThrow.com');
    await expect(await testFunction()).toThrowError('Could not load info');
  });
});
