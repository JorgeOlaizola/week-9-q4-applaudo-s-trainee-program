import TDetail from '../interfaces/detail/TDetail';
import { REQUEST_URL } from '../utils/env';
import { get } from './Fetch';
import { ICharacterDetail } from '../interfaces/detail/ICharacterDetail';
import { IComicDetail } from '../interfaces/detail/IComicDetail';
import { IStoryDetail } from '../interfaces/detail/IStoryDetail';

const fetchCharacterDetail = (id: string) => {
  return get<ICharacterDetail>(REQUEST_URL(`characters/${id}`)).then((response) => charactersHandler(response));
};

const charactersHandler = (response: ICharacterDetail) => {
  return response.data.results.map((item) => {
    return {
      id: item.id,
      name: item.name,
      description: item.description,
      source: item.urls[0].url,
      img: `${item.thumbnail.path}.${item.thumbnail.extension}`,
      comics: item.comics.available ? item.comics.items : null,
      stories: item.stories.available ? item.stories.items : null,
    };
  });
};

const fetchComicDetail = (id: string) => {
  return get<IComicDetail>(REQUEST_URL(`comics/${id}`)).then((response) => comicsHandler(response));
};

const comicsHandler = (response: IComicDetail) => {
  return response.data.results.map((item) => {
    return {
      id: item.id,
      title: item.title,
      description: item.description,
      source: item.urls[0].url,
      img: `${item.thumbnail.path}.${item.thumbnail.extension}`,
      stories: item.stories.available ? item.stories.items : null,
      characters: item.characters.available ? item.characters.items : null,
    };
  });
};

const fetchStoryDetail = (id: string) => {
  return get<IStoryDetail>(REQUEST_URL(`stories/${id}`)).then((response) => storyHandler(response));
};

const storyHandler = (response: IStoryDetail) => {
  return response.data.results.map((item) => {
    return {
      id: item.id,
      title: item.title,
      description: item.description,
      characters: item.characters.available ? item.characters.items : null,
      comics: item.comics.available ? item.comics.items : null,
    };
  });
};

const fetchDetail = (id: string, type: TDetail) => {
  switch (type) {
    case 'characters':
      return fetchCharacterDetail(id);
    case 'stories':
      return fetchStoryDetail(id);
    case 'comics':
      return fetchComicDetail(id);
    default:
      return null;
  }
};

export default fetchDetail;
